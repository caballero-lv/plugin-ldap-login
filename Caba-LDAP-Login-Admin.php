<?php
global $CabaLDAPLogin;

if( isset( $_GET[ 'tab' ] ) ) {
    $active_tab = $_GET[ 'tab' ];
} else {
	$active_tab = 'caba';
}
?>
<div class="wrap">

    <div id="icon-themes" class="icon32"></div>
    <h2>Caba LDAP Login Settings</h2>

    <h2 class="nav-tab-wrapper">
        <a href="<?php echo add_query_arg( array('tab' => 'caba'), $_SERVER['REQUEST_URI'] ); ?>" class="nav-tab <?php echo $active_tab == 'caba' ? 'nav-tab-active' : ''; ?>">Caba</a>
        <a href="<?php echo add_query_arg( array('tab' => 'advanced'), $_SERVER['REQUEST_URI'] ); ?>" class="nav-tab <?php echo $active_tab == 'advanced' ? 'nav-tab-active' : ''; ?>">Advanced</a>
    </h2>

    <form method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
    	<?php wp_nonce_field( 'save_sll_settings','save_the_sll' ); ?>

    	<?php if( $active_tab == "caba" ): ?>
    	<h3>Required settings</h3>
    	<table class="form-table">
			<tbody>
				<tr>
					<th scope="row" valign="top">Enable LDAP Authentication</th>
					<td>
						<input type="hidden" name="<?php echo $this->get_field_name('enabled'); ?>" value="false" />
						<label><input type="checkbox" name="<?php echo $this->get_field_name('enabled'); ?>" value="true" <?php if( str_true($this->get_setting('enabled')) ) echo "checked"; ?> /> on/off</label><br/>
					</td>
	    		<tr>
	    		<tr>
					<th scope="row" valign="top">Account Suffix</th>
					<td>
						<input type="text" name="<?php echo $this->get_field_name('account_suffix'); ?>" value="<?php echo $CabaLDAPLogin->get_setting('account_suffix'); ?>" /><br/>
						Often the suffix of your e-mail address. Example: @gmail.com
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top">Base DN</th>
					<td>
						<input type="text" name="<?php echo $this->get_field_name('base_dn'); ?>" value="<?php echo $CabaLDAPLogin->get_setting('base_dn'); ?>" />
						<br/>
						Example: For subdomain.domain.suffix, use DC=subdomain,DC=domain,DC=suffix. Do not specify an OU here.
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top">Domain Controller(s)</th>
					<td>
						<input type="text" name="<?php echo $this->get_field_name('domain_controllers', 'array'); ?>" value="<?php echo join(';', (array)$CabaLDAPLogin->get_setting('domain_controllers')); ?>" />
						<br/>Separate with semi-colons.
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top">LDAP Directory</th>
					<td>
						<label><input type="radio" name="<?php echo $this->get_field_name('directory'); ?>" value="ad" <?php if( $this->get_setting('directory') == "ad" ) echo "checked"; ?> /> Active Directory</label><br/>
						<label><input type="radio" name="<?php echo $this->get_field_name('directory'); ?>" value="ol" <?php if( $this->get_setting('directory') == "ol" ) echo "checked"; ?> /> Open LDAP (and etc)</label>
					</td>
				</tr>
			</tbody>
    	</table>
    	<p><input class="button-primary" type="submit" value="Save Settings" /></p>
    	<?php elseif ( $active_tab == "advanced" ): ?>
    	<h3>Settings</h3>
    	<table class="form-table" style="margin-bottom: 20px;">
			<tbody>
				<tr>
					<th scope="row" valign="top">Required Groups</th>
					<td>
						<input type="text" name="<?php echo $this->get_field_name('groups', 'array'); ?>" value="<?php echo join(';', (array)$CabaLDAPLogin->get_setting('groups')); ?>" /><br/>
						LDAP auth groups. <br/>
						Separate with semi-colons.
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top">LDAP Exclusive</th>
					<td>
						<input type="hidden" name="<?php echo $this->get_field_name('high_security'); ?>" value="false" />
						<label><input type="checkbox" name="<?php echo $this->get_field_name('high_security'); ?>" value="true" <?php if( str_true($this->get_setting('high_security')) ) echo "checked"; ?> /> Force all logins to LDAP. </label><br/>
					</td>
				</tr>
				<tr>
					<th scope="row" valign="top">User Creations</th>
					<td>
						<input type="hidden" name="<?php echo $this->get_field_name('create_users'); ?>" value="false" />
						<label><input type="checkbox" name="<?php echo $this->get_field_name('create_users'); ?>" value="true" <?php if( str_true($this->get_setting('create_users')) ) echo "checked"; ?> /> Auto create WordPress users.</label><br/>
					</td>
	    		<tr>
					<th scope="row" valign="top">Force login as user</th>
					<td>
						<input type="text" name="<?php echo $this->get_field_name('flogin_user'); ?>" value="<?php echo $CabaLDAPLogin->get_setting('flogin_user'); ?>" /><br/>
						Force login as user ignoring any other setings
					</td>
				</tr>
	    		<tr>
					<th scope="row" valign="top">New User Role</th>
					<td>
						<select name="<?php echo $this->get_field_name('role'); ?>">
							<?php wp_dropdown_roles( strtolower($this->get_setting('role')) ); ?>
						</select>
					</td>
				</tr>
			</tbody>
    	</table>
    	<hr />

    	<p><input class="button-primary" type="submit" value="Save Settings" /></p>
    	<?php else: ?>

    	<?php endif; ?>
    </form>
</div>
